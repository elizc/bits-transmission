package receivers;

import company.Util;

public class HemmingReceiver extends Receiver {
    private final int RESULT_BITS_LENGTH = 7;
    public HemmingReceiver(byte[] bits) {
        super(bits);
    }

    public byte[] getInfoBits() {
        byte[] bits = super.getBits();
        byte[] infoBits = new byte[Util.INFO_BITS_LENGTH];
        infoBits[0] = bits[2];
        System.arraycopy(bits, 4, infoBits, 1, infoBits.length - 1);
        return infoBits;
    }

    public boolean isCorrect() {
        if (hasError() && checkParity()) {
            System.out.println("Найдена двойная ошибка");
            return false;
        }

        if (hasError() && !checkParity()) {
            fixError();
            System.out.println("Найдена и исправлена одиночная ошибка");
            return true;
        }

        return true;
    }

    private void fixError() {
       int errorPosition = findErrorPosition();
       if (super.getBits()[errorPosition] == 1) {
           super.getBits()[errorPosition] = 0;
       } else {
           super.getBits()[errorPosition] = 1;
       }
    }

    private int findErrorPosition() {
        byte[] bits = super.getBits();
        byte[] checkBits = getCheckBits();
        int errorPosition = 0;

        if (checkBits[0] != bits[0]) {
            errorPosition++;
        }
        if (checkBits[1] != bits[1]) {
            errorPosition += 2;
        }
        if (checkBits[2] != bits[3]) {
            errorPosition += 4;
        }
        return errorPosition - 1;
    }

    private boolean hasError() {
        byte[] bits = super.getBits();
        byte[] checkBits = getCheckBits();
        return !(checkBits[0] == bits[0] && checkBits[1] == bits[1] && checkBits[2] == bits[3]);
    }

    private boolean checkParity() {
        byte[] bits = super.getBits();
        return getParityBit() == bits[RESULT_BITS_LENGTH];
    }

    private byte[] getCheckBits() {
        byte[] bits = super.getBits();
        byte[] checkBits = new byte[3];
        checkBits[0] = (byte)(bits[2] ^ bits[4] ^ bits[6]);
        checkBits[1] = (byte)(bits[2] ^ bits[5] ^ bits[6]);
        checkBits[2] = (byte)(bits[4] ^ bits[5] ^ bits[6]);
        return checkBits;
    }

    private byte getParityBit() {
        byte[] bits = super.getBits();
        byte parityBit = 0;
        for (int i = 0; i < RESULT_BITS_LENGTH; i++) {
            parityBit ^= bits[i];
        }
        return parityBit;
    }
}
