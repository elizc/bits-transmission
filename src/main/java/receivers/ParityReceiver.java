package receivers;

import java.util.Arrays;

public class ParityReceiver extends Receiver {
    public ParityReceiver(byte[] bits) {
        super(bits);
    }

    public boolean isCorrect() {
        byte[] bits = super.getBits();
        byte checkBit = 0;
        for (byte bit: bits) {
            checkBit ^= bit;
        }
        return checkBit == 0;
    }

    public byte[] getInfoBits() {
        byte[] bits = super.getBits();
        return Arrays.copyOf(bits, bits.length - 1);
    }
}
