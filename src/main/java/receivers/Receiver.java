package receivers;

public abstract class Receiver {
    private byte[] bits;

    public Receiver(byte[] bits) {
        this.bits = bits;
    }

    public abstract byte[] getInfoBits();

    public abstract boolean isCorrect();

    public byte[] getBits() {
        return bits;
    }
}
