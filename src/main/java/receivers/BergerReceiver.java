package receivers;

import company.Util;

import java.util.Arrays;

public class BergerReceiver extends Receiver {
    private final int RESULT_BITS_LENGTH = 7;

    public BergerReceiver(byte[] bits) {
        super(bits);
    }

    public byte[] getInfoBits() {
        byte[] bits = super.getBits();
        return Arrays.copyOf(bits, Util.INFO_BITS_LENGTH);
    }

    public boolean isCorrect() {
        byte[] bits = super.getBits();
        byte[] checkBits = getCheckBits();
        byte[] receivedCheckBits = Arrays.copyOfRange(bits, Util.INFO_BITS_LENGTH, bits.length);
        return Arrays.compare(checkBits, receivedCheckBits) == 0;
    }

    private int countInfoOnes() {
        byte[] bits = super.getBits();
        int onesCount = 0;
        for (int i = 0; i < Util.INFO_BITS_LENGTH; i++) {
            if (bits[i] == 1) onesCount++;
        }
        return onesCount;
    }

    private byte[] getCheckBits() {
        int infoOnesCount = countInfoOnes();
        String binaryOnesCount = Integer.toString(infoOnesCount, 2);
        if (binaryOnesCount.length() < 2) binaryOnesCount = "0" + binaryOnesCount;
        if (binaryOnesCount.length() < 3) binaryOnesCount = "0" + binaryOnesCount;
        return revert(Util.getBitsFromString(binaryOnesCount));
    }

    private byte[] revert(byte[] bits) {
        for (int i = 0; i < bits.length; i++) {
            if (bits[i] == 0) {
                bits[i] = 1;
            } else {
                bits[i] = 0;
            }
        }
        return bits;
    }
}
