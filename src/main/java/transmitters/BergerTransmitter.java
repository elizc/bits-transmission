package transmitters;

import company.Util;

public class BergerTransmitter extends Transmitter {

    public BergerTransmitter(byte[] bits) {
        super(bits);
    }

    public byte[] transmit() {
        int RESULT_BITS_LENGTH = 7;
        byte[] bits = super.getBits();
        byte[] resultBits = new byte[RESULT_BITS_LENGTH];
        System.arraycopy(bits, 0, resultBits, 0, bits.length);

        byte[] checkBits = getCheckBits();
        System.arraycopy(checkBits, 0, resultBits, 4, checkBits.length);
        return resultBits;
    }

    private byte[] getCheckBits() {
        int onesCount = countOnes();
        String binaryOnesCount = Integer.toString(onesCount, 2);
        if (binaryOnesCount.length() < 2) binaryOnesCount = "0" + binaryOnesCount;
        if (binaryOnesCount.length() < 3) binaryOnesCount = "0" + binaryOnesCount;
        return revert(Util.getBitsFromString(binaryOnesCount));
    }

    private byte[] revert(byte[] bits) {
        for (int i = 0; i < bits.length; i++) {
            if (bits[i] == 0) {
                bits[i] = 1;
            } else {
                bits[i] = 0;
            }
        }
        return bits;
    }

    private int countOnes() {
        int onesCount = 0;
        byte[] bits = super.getBits();
        for (byte bit: bits) {
            if (bit == 1) onesCount++;
        }
        return onesCount;
    }
}
