package transmitters;

public class ParityTransmitter extends Transmitter {
    public ParityTransmitter(byte[] bits) {
        super(bits);
    }

    public byte[] transmit() {
        byte[] bits = super.getBits();
        byte parityBit = getParityBit();
        byte[] resultBits = new byte[bits.length + 1];
        System.arraycopy(bits, 0, resultBits, 0, bits.length);
        resultBits[resultBits.length - 1] = parityBit;
        return resultBits;
    }

    private byte getParityBit() {
        byte parityBit = 0;
        byte[] bits = super.getBits();
        for (byte bit: bits) {
            parityBit ^= bit;
        }
        return parityBit;
    }
}
