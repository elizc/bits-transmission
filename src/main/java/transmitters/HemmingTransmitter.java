package transmitters;

public class HemmingTransmitter extends Transmitter {

    public HemmingTransmitter(byte[] bits) {
        super(bits);
    }

    public byte[] transmit() {
        int RESULT_BITS_LENGTH = 7;
        byte[] bits = super.getBits();
        byte[] resultBits = new byte[RESULT_BITS_LENGTH + 1];
        if (bits.length - 1 >= 0) System.arraycopy(bits, 1, resultBits, 4, bits.length - 1);
        resultBits[2] = bits[0];

        byte[] checkBits = getCheckBits();
        resultBits[0] = checkBits[0];
        resultBits[1] = checkBits[1];
        resultBits[3] = checkBits[2];
        resultBits[RESULT_BITS_LENGTH] = getParityBit(resultBits);

        return resultBits;
    }

    private byte[] getCheckBits() {
        byte[] bits = super.getBits();
        byte[] checkBits = new byte[3];
        checkBits[0] = (byte)(bits[0] ^ bits[1] ^ bits[3]);
        checkBits[1] = (byte)(bits[0] ^ bits[2] ^ bits[3]);
        checkBits[2] = (byte)(bits[1] ^ bits[2] ^ bits[3]);
        return checkBits;
    }

    private byte getParityBit(byte[] bits) {
        byte parityBit = 0;
        for (byte bit: bits) {
            parityBit ^= bit;
        }
        return parityBit;
    }
}
