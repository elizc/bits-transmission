package transmitters;

public abstract class Transmitter {
    private byte[] bits;

    public Transmitter(byte[] bits) {
        this.bits = bits;
    }

    public abstract byte[] transmit();

    public byte[] getBits() {
        return bits;
    }
}
