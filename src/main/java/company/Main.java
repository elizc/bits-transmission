package company;

import fabrics.IFactory;
import receivers.Receiver;
import transmitters.Transmitter;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        System.out.println("Введите послеовательность из 4 бит, сотоящую только из 0 и 1");
        byte[] bits = IO.readBits();

        System.out.println("Выберите метод кодирования (parity, berger или hemming)");
        String method = IO.readMethod();
        IFactory factory = Util.chooseFactory(method);

        simulate(bits, factory);
    }

    private static void simulate(byte[] bits, IFactory factory) {
        Transmitter transmitter = factory.createTransmitter(bits);
        byte[] transmittedBits = transmitter.transmit();
        System.out.println("Отправлены биты " + Arrays.toString(transmittedBits) + "\n");

        byte[] corruptedBits = Environment.changeBits(transmittedBits, 1);
        System.out.println("В ходе передачи изменился один бит, \nи в результате получено " + Arrays.toString(corruptedBits));

        Receiver reciever = factory.createReciever(corruptedBits);
        IO.printResult(reciever);

        corruptedBits = Environment.changeBits(transmittedBits, 2);
        System.out.println("В ходе передачи изменилось два бита, \nи в результате получено " + Arrays.toString(corruptedBits));

        reciever = factory.createReciever(corruptedBits);
        IO.printResult(reciever);
    }
}
