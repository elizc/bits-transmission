package company;

import receivers.Receiver;

import java.util.Arrays;
import java.util.Scanner;

class IO {
    static void printResult(Receiver reciever) {
        if (reciever.isCorrect()) {
            System.out.println("Информационные биты " + Arrays.toString(reciever.getInfoBits()) + " верны");
        } else {
            System.out.println("В информационных битах " + Arrays.toString(reciever.getInfoBits()) + " есть ошибка");
        }
        System.out.println();
    }

    static byte[] readBits() {
        Scanner scanner = new Scanner(System.in);
        String bitsString;
        while (true) {
            bitsString = scanner.nextLine();
            if (!Util.checkBitsString(bitsString)) {
                System.out.println("Строка должна быть из 4 символов и соcтоять только из 0 и 1");
            } else {
                break;
            }
        }
        return Util.getBitsFromString(bitsString);
    }

    static String readMethod() {
        Scanner scanner = new Scanner(System.in);
        String method;
        while (true) {
            method = scanner.nextLine().toLowerCase();
            if (!(method.equals("parity") || method.equals("berger") || method.equals("hemming"))) {
                System.out.println("Метод кодирования должен быть parity, berger или hemming");
            } else {
                break;
            }
        }
        return method;
    }
}
