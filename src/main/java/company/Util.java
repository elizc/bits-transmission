package company;

import fabrics.BergerFactory;
import fabrics.HemmingFactory;
import fabrics.IFactory;
import fabrics.ParityFactory;

import java.util.regex.Pattern;

public class Util {
    public static final int INFO_BITS_LENGTH = 4;

    public static byte[] getBitsFromString(String bitsString) {
        byte[] bits = new byte[bitsString.length()];
        for (int i = 0; i < bitsString.length(); i++) {
            switch (bitsString.charAt(i)) {
                case '0': {
                    bits[i] = 0;
                    break;
                }
                case '1': {
                    bits[i] = 1;
                    break;
                }
                default: {
                    break;
                }
            }
        }
        return bits;
    }

    static boolean checkBitsString(String bitsString) {
        String pattern = "[01]{4}"; //Only four 1 or 0
        return Pattern.matches(pattern, bitsString);
    }

    static IFactory chooseFactory(String method) {
        IFactory factory = null;
        switch (method) {
            case "parity": {
                factory = new ParityFactory();
                break;
            }
            case "berger": {
                factory = new BergerFactory();
                break;
            }
            case "hemming": {
                factory = new HemmingFactory();
                break;
            }
        }
        return factory;
    }
}
