package company;

import java.util.ArrayList;
import java.util.Arrays;

class Environment {
    private static ArrayList<Integer> usedPositions = new ArrayList<>();

    static byte[] changeBits(byte[] bits, int bitsCount) {
        byte[] changedBits = Arrays.copyOf(bits, bits.length);
        for (int i = 1; i <= bitsCount; i++) {
            int position;
            do {
                position = (int) Math.floor(Math.random() * changedBits.length);
            } while (usedPositions.contains(position));

            if (changedBits[position] == 1) {
                changedBits[position] = 0;
            } else {
                changedBits[position] = 1;
            }
            usedPositions.add(position);
        }

        usedPositions.clear();
        return changedBits;
    }
}
