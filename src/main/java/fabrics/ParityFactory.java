package fabrics;

import receivers.ParityReceiver;
import receivers.Receiver;
import transmitters.ParityTransmitter;
import transmitters.Transmitter;

public class ParityFactory implements IFactory {
    @Override
    public Transmitter createTransmitter(byte[] bits) {
        return new ParityTransmitter(bits);
    }

    @Override
    public Receiver createReciever(byte[] bits) {
        return new ParityReceiver(bits);
    }
}
