package fabrics;

import receivers.HemmingReceiver;
import receivers.Receiver;
import transmitters.HemmingTransmitter;
import transmitters.Transmitter;

public class HemmingFactory implements IFactory {
    @Override
    public Transmitter createTransmitter(byte[] bits) {
        return new HemmingTransmitter(bits);
    }

    @Override
    public Receiver createReciever(byte[] bits) {
        return new HemmingReceiver(bits);
    }
}
