package fabrics;

import receivers.BergerReceiver;
import receivers.Receiver;
import transmitters.BergerTransmitter;
import transmitters.Transmitter;

public class BergerFactory implements IFactory {
    @Override
    public Transmitter createTransmitter(byte[] bits) {
        return new BergerTransmitter(bits);
    }

    @Override
    public Receiver createReciever(byte[] bits) {
        return new BergerReceiver(bits);
    }
}
