package fabrics;

import receivers.Receiver;
import transmitters.Transmitter;

public interface IFactory {
    Transmitter createTransmitter(byte[] bits);
    Receiver createReciever(byte[] bits);
}
