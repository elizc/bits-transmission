package company;

import org.junit.Assert;
import org.junit.Test;

public class EnvironmentTest {
    private byte[] initialBits = {0, 0, 0, 0, 0, 0};
    private int[] changedBitsCounts = {-1, 0, 1, 2, 6};

    @Test
    public void changeBits() {
        for (int changedBitsCount : changedBitsCounts) {
            byte[] actualBits = Environment.changeBits(initialBits, changedBitsCount);
            if (changedBitsCount <= 0) {
                Assert.assertArrayEquals(initialBits, actualBits);
            } else {
                byte[] result = Environment.changeBits(initialBits, changedBitsCount);
                Assert.assertEquals(changedBitsCount, countChangedBits(result));
            }
        }
    }

    private int countChangedBits(byte[] bits) {
        int changedBitsCount = 0;
        for (int i = 0; i < bits.length; i++) {
            if (bits[i] != initialBits[i]) {
                changedBitsCount++;
            }
        }
        return changedBitsCount;
    }
}