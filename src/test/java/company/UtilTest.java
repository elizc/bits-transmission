package company;

import fabrics.BergerFactory;
import fabrics.HemmingFactory;
import fabrics.ParityFactory;
import org.junit.Assert;
import org.junit.Test;

public class UtilTest {
    @Test
    public void getBitsFromString() {
        Assert.assertArrayEquals(new byte[]{0, 0, 0, 0}, Util.getBitsFromString("0000"));
        Assert.assertArrayEquals(new byte[]{1, 1}, Util.getBitsFromString("11"));
        Assert.assertArrayEquals(new byte[]{1, 0, 0, 1, 0}, Util.getBitsFromString("10010"));
        Assert.assertArrayEquals(new byte[]{0, 0, 0, 0}, Util.getBitsFromString("6a&+"));
        Assert.assertArrayEquals(new byte[]{0, 1, 0, 0}, Util.getBitsFromString("610a"));
    }

    @Test
    public void checkBitsString() {
        Assert.assertTrue(Util.checkBitsString("1010"));
        Assert.assertTrue(Util.checkBitsString("0000"));
        Assert.assertTrue(Util.checkBitsString("1111"));

        Assert.assertFalse(Util.checkBitsString("101"));
        Assert.assertFalse(Util.checkBitsString("10110"));
        Assert.assertFalse(Util.checkBitsString("1"));
        Assert.assertFalse(Util.checkBitsString(""));
        Assert.assertFalse(Util.checkBitsString("af61"));
    }

    @Test
    public void chooseFactory() {
        Assert.assertEquals(ParityFactory.class.getName(), Util.chooseFactory("parity").getClass().getName());
        Assert.assertEquals(BergerFactory.class.getName(), Util.chooseFactory("berger").getClass().getName());
        Assert.assertEquals(HemmingFactory.class.getName(), Util.chooseFactory("hemming").getClass().getName());
        Assert.assertNull(Util.chooseFactory("unknown"));
    }
}