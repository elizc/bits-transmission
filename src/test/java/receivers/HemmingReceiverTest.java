package receivers;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

public class HemmingReceiverTest {
    private Map<byte[], Boolean> fixture = new HashMap<>();

    @Before
    public void setUp() {
        //no errors
        fixture.put(new byte[]{0, 0, 0, 0, 0, 0, 0, 0}, true);
        fixture.put(new byte[]{1, 1, 1, 1, 1, 1, 1, 1}, true);
        fixture.put(new byte[]{1, 1, 1, 0, 0, 0, 0, 1}, true);

        //1 error but it is fixed or does not influence
        fixture.put(new byte[]{0, 0, 1, 0, 0, 0, 0, 0}, true);
        fixture.put(new byte[]{0, 1, 0, 0, 0, 0, 0, 0}, true);
        fixture.put(new byte[]{0, 0, 0, 0, 0, 0, 0, 1}, true);

        //2 errors
        fixture.put(new byte[]{0, 0, 1, 1, 0, 0, 0, 0}, false);
        fixture.put(new byte[]{0, 1, 1, 0, 0, 0, 0, 0}, false);
        fixture.put(new byte[]{1, 1, 0, 0, 0, 0, 0, 0}, false);
        fixture.put(new byte[]{1, 0, 0, 0, 0, 0, 0, 1}, false);
        fixture.put(new byte[]{0, 0, 1, 0, 0, 0, 0, 1}, false);
    }

    @Test
    public void getInfoBits() {
        byte[] receivedBits = {1, 1, 0, 0, 1, 0, 1, 1};
        Receiver receiver = new HemmingReceiver(receivedBits);
        byte[] infoBits = {0, 1, 0, 1};
        Assert.assertArrayEquals(infoBits, receiver.getInfoBits());
    }

    @Test
    public void isCorrect() {
        for (Map.Entry<byte[], Boolean> fixtureEntry : fixture.entrySet()) {
            byte[] initialBits = fixtureEntry.getKey();
            boolean expectedResult = fixtureEntry.getValue();
            Receiver receiver = new HemmingReceiver(initialBits);
            Assert.assertEquals(expectedResult, receiver.isCorrect());
        }
    }
}