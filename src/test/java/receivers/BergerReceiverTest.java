package receivers;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

public class BergerReceiverTest {
    private Map<byte[], Boolean> fixture = new HashMap<>();

    @Before
    public void setUp() {
        fixture.put(new byte[]{0, 0, 0, 0, 1, 1, 1}, true);
        fixture.put(new byte[]{0, 0, 0, 0, 0, 0, 0}, false);
        fixture.put(new byte[]{1, 1, 1, 1, 0, 1, 1}, true);
        fixture.put(new byte[]{1, 1, 1, 1, 1, 1, 1}, false);
        fixture.put(new byte[]{1, 1, 0, 0, 1, 0, 1}, true);
        fixture.put(new byte[]{1, 1, 0, 1, 1, 0, 1}, false);
    }

    @Test
    public void getInfoBits() {
        byte[] receivedBits = {1, 1, 0, 0, 1, 0, 0};
        Receiver receiver = new BergerReceiver(receivedBits);
        byte[] infoBits = {1, 1, 0, 0};
        Assert.assertArrayEquals(infoBits, receiver.getInfoBits());
    }

    @Test
    public void isCorrect() {
        for (Map.Entry<byte[], Boolean> fixtureEntry : fixture.entrySet()) {
            byte[] initialBits = fixtureEntry.getKey();
            boolean expectedResult = fixtureEntry.getValue();
            Receiver receiver = new BergerReceiver(initialBits);
            Assert.assertEquals(expectedResult, receiver.isCorrect());
        }
    }
}