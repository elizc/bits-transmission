package transmitters;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

public class BergerTransmitterTest {

    private Map<byte[], byte[]> fixture = new HashMap<>();

    @Before
    public void setUp() {
        fixture.put(new byte[]{0, 0, 0, 0}, new byte[]{0, 0, 0, 0, 1, 1, 1});
        fixture.put(new byte[]{1, 0, 0, 0}, new byte[]{1, 0, 0, 0, 1, 1, 0});
        fixture.put(new byte[]{1, 1, 0, 0}, new byte[]{1, 1, 0, 0, 1, 0, 1});
        fixture.put(new byte[]{1, 1, 1, 1}, new byte[]{1, 1, 1, 1, 0, 1, 1});
    }

    @Test
    public void transmit() {
        for (Map.Entry<byte[], byte[]> fixtureEntry: fixture.entrySet()) {
            byte[] initialBits = fixtureEntry.getKey();
            byte[] expectedBits = fixtureEntry.getValue();
            Transmitter transmitter = new BergerTransmitter(initialBits);
            Assert.assertArrayEquals(expectedBits, transmitter.transmit());
        }
    }
}