package transmitters;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

public class ParityTransmitterTest {
    private Map<byte[], byte[]> fixture = new HashMap<>();

    @Before
    public void setUp() {
        fixture.put(new byte[]{0, 0, 0, 0}, new byte[]{0, 0, 0, 0, 0});
        fixture.put(new byte[]{1, 0, 0, 0}, new byte[]{1, 0, 0, 0, 1});
        fixture.put(new byte[]{1, 1, 0, 0}, new byte[]{1, 1, 0, 0, 0});
        fixture.put(new byte[]{1, 1, 1, 1}, new byte[]{1, 1, 1, 1, 0});
    }

    @Test
    public void testTransmit() {
        for (Map.Entry<byte[], byte[]> fixtureEntry : fixture.entrySet()) {
            byte[] initialBits = fixtureEntry.getKey();
            byte[] expectedBits = fixtureEntry.getValue();
            Transmitter transmitter = new ParityTransmitter(initialBits);
            Assert.assertArrayEquals(expectedBits, transmitter.transmit());
        }
    }
}