В программе моделируется передача битов с одной и с двумя ошибками. Для проверки надо используются методы Бергера, Хемминга и проверки чётности.

**1.	 Проверка чётности**

В каждом пакете данных есть один бит четности. Он рассчитывается как сумма по модулю два всех битов сообщения и добавляется в его конец, в результате в «закодированном» сообщении число единиц всегда будет чётным. После этого одиночная ошибка может быть легко определена подсчётом количества единиц сложением по модулю два в принятом сообщении сложением по модулю два. Если число единиц в сообщении чётно, то результатом сложения будет ноль, и это означает, что ошибки нет.

**2.	 Код Бергера**

В информационной части кода подсчитывается число единиц, после чего добавляются проверочные разряды, представляющие инвертированную запись этого числа в двоичной форме. После получения сообщения снова подсчитываются проверочные разряды, и если они совпадают с теми, что находятся в сообщении, то ошибки нет

**3.	 Код Хемминга**

Для каждого числа проверочных символов используется специальная маркировка вида (k, i), где k — количество символов в сообщении, i — количество информационных символов в сообщении. В данной работе используется код (7, 4). 

Для кодирования всех битов 4-битового сообщения необходимо распределить его биты по позициям 2, 4, 5 и 6, затем вычислить контрольные биты по специальным формулам

*Бит 0 = Бит 2 ^ Бит 4 ^ Бит 6*

*Бит 1 = Бит 2 ^ Бит 5 ^ Бит 6*

*Бит 3 = Бит 4 ^ Бит 5 ^ Бит 6*


После передачи 7-битового сообщения по нему снова формируются контрольные биты по формулам (1). Если они совпадают с контрольными битами в сообщении, то ошибки нет.

В программе этот метод был расширен: одиночная ошибка исправляется и может обнаружиться двойная

Контрольные биты в полученном сообщении находятся на позициях 0, 1 и 3. Также даны контрольные биты, подсчитанные для полученных информационных битов. Позиция ошибки вычисляется следующим образом:

1. Изначально она считается равной 0
2. Если не совпадают первые контрольные биты, прибавить к ней 1
3. Если не совпадают вторые контрольные биты, прибавить к ней 2
4. Если не совпадают третьи контрольные биты, прибавить к ней 4
5. Вычесть из результата 1

Ошибка исправляется инвертированием бита в найденной позиции.

Для обнаружения двойной ошибки перед отправкой сообщения надо к нему добавить восьмой бит чётности. Тогда при получении сообщения необходимо действовать следующим образом:

1. Если контрольные биты совпадают, ошибки нет
2. Если контрольные биты не совпадают и чётность нарушена, то есть одна ошибка, которую можно найти и исправить
3. Если контрольные биты не совпадают и чётность сохранена, то есть две ошибки.

